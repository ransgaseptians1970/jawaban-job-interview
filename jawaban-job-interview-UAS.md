# 1. Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital:

- **Use case user**



| Usecase Google Drive  | Nilai |
| ------ | ------ |
|     1. Membuat Folder:    |    90    |
|   2. Berbagi Folder dan File:     |     80   |
| 3. Mengelola Akses: | 75|
| 4. Mengunggah  Mengedit dan menghapus File: |75|
|5. Penyimpanan |70 |
|6. Menghapus folder |60 |
|7. Notifikasi |50 |
|8. menampilkan file di dalan folder  |40 |
|9. Megunduh File|30|



- **Use case manajemen perusahaan**

| Usecase Google Drive  | Nilai |
| ------ | ------ |
|1.  Mengedit Akun User |95|
|2.  Mengelola Akses: | 90|


- **Use case direksi perusahaan (dashboard, monitoring, analisis)**


| Usecase Google Drive  | Nilai |
| ------ | ------ |
|1.  Mengedit Akun User |95|
|2.  Membuat Folder:    |    90    |
|3.  Berbagi Folder dan File:     |     80   |
|4.  Mengelola Akses: | 75|
|5.  Mengunggah  Mengedit dan menghapus File: |75|
|6.  Penyimpanan |70 |
|7.  Menghapus folder |60 |
|8.  Notifikasi |50 |
|9.  Menampilkan file di dalan folder  |40 |
|10. Megunduh File|30|

# 2. Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital

```mermaid
classDiagram
    class GoogleDrive {
        +listFiles()
        +uploadFile()
        +downloadFile()
        +shareFile()
    }

    class User {
        -name: string
        -email: string
        +login()
        +logout()
        +uploadFile()
        +downloadFile()
        +shareFile()
    }

    class File {
        -name: string
        -size: int
    }

    GoogleDrive --> User : Manages
    GoogleDrive --> File : Contains

```




# 3. Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle

- **Prinsip Single Responsibility (SRP)**: Setiap kelas sebaiknya memiliki satu tanggung jawab tunggal.

kelas `FileSizeCalculator` dan `FileSizeFormatter` dapat mematuhi prinsip SRP. Jika ada beberapa tanggung jawab dalam kelas-kelas tersebut, mempertimbangkan untuk membaginya menjadi kelas-kelas terpisah dengan tanggung jawab yang jelas.

```
<?php
interface FileSizeCalculatorInterface
{
    public function getUsedSpace();
}

class FileSizeCalculator implements FileSizeCalculatorInterface
{
    private $directory;

    public function __construct($directory)
    {
        $this->directory = $directory;
    }

    public function getUsedSpace()
    {
      
        $folders = array_filter(glob($this->directory . '*'), 'is_dir');
        $totalSize = 0;

        foreach ($folders as $folder) {
            $files = glob($folder . '/*');
            foreach ($files as $file) {
                $totalSize += filesize($file);
            }
        }

        return $totalSize;
    }
}
```
- **Prinsip Open-Closed (OCP)**: Entitas perangkat lunak sebaiknya terbuka untuk perluasan, tetapi tertutup untuk modifikasi.

Jika di masa depan semisal ingin menambahkan format ukuran file baru, pastikan `FileSizeFormatter` dapat diperluas tanpa mengubah kode yang ada.


```
class FileSizeFormatter
{
    public static function formatSizeUnits($bytes)
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');
        $index = 0;

        while ($bytes >= 1024 && $index < 4) {
            $bytes /= 1024;
            $index++;
        }

        return round($bytes, 2) . ' ' . $units[$index];
    }
}

$directory = 'uploads/';
$fileSizeCalculator = new FileSizeCalculator($directory);
$usedSpace = $fileSizeCalculator->getUsedSpace();
$usedSpaceFormatted = FileSizeFormatter::formatSizeUnits($usedSpace);
?>
```
- **Prinsip Liskov Substitution (LSP)**: Objek dari superclass harus dapat digantikan oleh objek dari subclass tanpa mempengaruhi kebenaran program.

ada subclass yang mengimplementasikan `FileSizeCalculatorInterface`,dapat digunakan sebagai pengganti objek `FileSizeCalculator` tanpa melanggar kebenaran program.

```
class FileSizeCalculator implements FileSizeCalculatorInterface
{
    private $directory;

    public function __construct($directory)
    {
        $this->directory = $directory;
    }

    public function getUsedSpace()
    {
      
        $folders = array_filter(glob($this->directory . '*'), 'is_dir');
        $totalSize = 0;

        foreach ($folders as $folder) {
            $files = glob($folder . '/*');
            foreach ($files as $file) {
                $totalSize += filesize($file);
            }
        }

        return $totalSize;
    }
}
```
- **Prinsip Interface Segregation (ISP)**: Klien tidak boleh dipaksa bergantung pada antarmuka yang tidak digunakan.

Antarmuka pada `FileSizeCalculatorInterface` hanya mencakup metode yang relevan dan diperlukan bagi klien yang menggunakannya. Hindari adanya metode yang tidak relevan.

```
<?php

interface FileSizeCalculatorInterface
{
    public function getUsedSpace();
}

class FileSizeCalculator implements FileSizeCalculatorInterface
{
    private $directory;

    public function __construct($directory)
    {
        $this->directory = $directory;
    }

    public function getUsedSpace()
    {
      
        $folders = array_filter(glob($this->directory . '*'), 'is_dir');
        $totalSize = 0;

        foreach ($folders as $folder) {
            $files = glob($folder . '/*');
            foreach ($files as $file) {
                $totalSize += filesize($file);
            }
        }

        return $totalSize;
    }
}

class FileSizeFormatter
{
    public static function formatSizeUnits($bytes)
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');
        $index = 0;

        while ($bytes >= 1024 && $index < 4) {
            $bytes /= 1024;
            $index++;
        }

        return round($bytes, 2) . ' ' . $units[$index];
    }
}

$directory = 'uploads/';
$fileSizeCalculator = new FileSizeCalculator($directory);
$usedSpace = $fileSizeCalculator->getUsedSpace();
$usedSpaceFormatted = FileSizeFormatter::formatSizeUnits($usedSpace);
?>
```
- **Prinsip Dependency Inversion (DIP)**: Bergantung pada abstraksi, bukan pada implementasi.

Penggunaan antarmuka **FileSizeCalculatorInterface** untuk mengurangi ketergantungan langsung pada implementasi **FileSizeCalculator**. Ini memungkinkan penggantian implementasi **FileSizeCalculator** dengan implementasi lain yang sesuai tanpa mempengaruhi klien yang menggunakannya.


```
class FileSizeCalculator implements FileSizeCalculatorInterface
{
    private $directory;

    public function __construct($directory)
    {
        $this->directory = $directory;
    }

    public function getUsedSpace()
    {
      

        $folders = array_filter(glob($this->directory . '*'), 'is_dir');
        $totalSize = 0;

        foreach ($folders as $folder) {
            $files = glob($folder . '/*');
            foreach ($files as $file) {
                $totalSize += filesize($file);
            }
        }

        return $totalSize;
    }
}
```


# 4. Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih

1. **Singleton Pattern:**
Design pattern Singleton digunakan untuk memastikan bahwa hanya ada satu instance dari sebuah kelas yang dapat diakses secara global.

Dalam kode tersebut, terdapat penggunaan Singleton pada kelas `FileSizeCalculator`. Singleton digunakan untuk memastikan bahwa hanya ada satu objek `FileSizeCalculator` yang digunakan untuk menghitung ruang yang digunakan oleh file.

```
class FileSizeCalculator implements FileSizeCalculatorInterface
{
    private $directory;

    public function __construct($directory)
    {
        $this->directory = $directory;
    }

    public function getUsedSpace()
    {
        $folders = array_filter(glob($this->directory . '*'), 'is_dir');
        $totalSize = 0;

        foreach ($folders as $folder) {
            $files = glob($folder . '/*');
            foreach ($files as $file) {
                $totalSize += filesize($file);
            }
        }

        return $totalSize;
    }
}

class FileSizeFormatter
{
    public static function formatSizeUnits($bytes)
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');
        $index = 0;

        while ($bytes >= 1024 && $index < 4) {
            $bytes /= 1024;
            $index++;
        }

        return round($bytes, 2) . ' ' . $units[$index];
    }
}
```
2. **Factory Pattern:**

Factory pattern adalah design pattern creational yang digunakan untuk membuat objek tanpa mengekspos logika pembuatannya. Dalam kode tersebut, terdapat penggunaan Factory Pattern dalam pembuatan instanc objek`FileSizeCalculator`. 

Dengan menggunakan Factory Pattern, Anda dapat membuat instance `FileSizeCalculator` melalui factory tanpa perlu mengekspos logika pembuatannya secara langsung.




```
interface FileSizeCalculatorInterface
{
    public function getUsedSpace();
}

class FileSizeCalculator implements FileSizeCalculatorInterface
{
    private $directory;

    public function __construct($directory)
    {
        $this->directory = $directory;
    }

    public function getUsedSpace()
    {

    }
}
```
3. **Observer Pattern:**

Observer pattern adalah design pattern behavioral yang digunakan untuk mengatur hubungan antara objek-objek yang bergantung pada satu objek subjek. 

Dalam kode tersebut, tidak terlihat secara langsung, tetapi penggunaan event handling atau callback dapat dikategorikan sebagai Observer Pattern. Misalnya, saat tombol "Upload" ditekan, fungsi` $_POST['upload']` dijalankan untuk memperbarui tampilan atau melakukan tindakan lain.


```
<?php
// Fungsi Upload File
if (isset($_POST['upload'])) {
  $file = $_FILES['file'];
  $fileName = $file['name'];
  $fileTmp = $file['tmp_name'];
  $fileSize = $file['size'];
  $fileError = $file['error'];
  $folder = $_POST['folder'];

  if ($fileError === 0) {
    $destination = $directory . $folder . '/' . $fileName;
    move_uploaded_file($fileTmp, $destination);
    echo '<div class="alert alert-success">File berhasil diupload.</div>';
  } else {
    echo '<div class="alert alert-danger">Terjadi kesalahan saat mengupload file.</div>';
  }
}
?>
```

# 5. Mampu menunjukkan dan menjelaskan konektivitas ke database

- PhpMyadmin

![image.png](Domentasi/database2.png)


- Koneksi.php

![image.png](Domentasi/Database.png)

Kode di atas berfungsi untuk melakukan otentikasi pengguna dengan memeriksa username dan password yang diberikan melalui formulir login dengan data yang ada di tabel "user" dalam database.

Dengan menambahkan langkah-langkah di atas,akan memiliki koneksi database yang aktif yang dapat digunakan untuk menjalankan kueri otentikasi dan memeriksa apakah pengguna berhasil masuk atau tidak.

# 6. Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya

- **Web service yang dipakai**

Slim FrameWork V3
![image.png](Domentasi/slimku.png)


- **Kode Yang Terkait Menggunakan FrameWork SlimV3**
1. Fungsi Hapus

```
<?php
require 'vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App();

$app->get('/', function (Request $request, Response $response) {
    $file = $request->getQueryParam('file');

    if ($file) {
        $filePath = 'uploads/' . $file;

        // Mendapatkan path absolut ke file
        $realPath = realpath($filePath);

        // Memastikan file ada dan diizinkan untuk dihapus
        if ($realPath && is_file($realPath)) {
            if (unlink($realPath)) {
                // File berhasil dihapus
                return $response->withRedirect('index.php');
            } else {
                // Gagal menghapus file
                return $response->getBody()->write('Gagal menghapus file.');
            }
        } else {
            // File tidak ditemukan
            return $response->getBody()->write('File tidak ditemukan.');
        }
    } else {
        // File tidak ditemukan
        return $response->getBody()->write('File tidak ditemukan.');
    }
});

$app->run();
```





- **Web Page Untuk User**

![image.png](Domentasi/websrvice.png)


- **Web Service Untuk Aplikasi**
![image.png](Domentasi/webapp.png)


**Tampilan CRUD**

CRUD disini berfungsi untuk membuat akun,memlihat akun,meng-update akun,dan mendelete akun.

- **Creat**

User dapat membuat akun baru dengan cara register dan memasukan username dan password lalu akan di proses di file p_register.php

```
<?php
include "koneksi.php";


// Memeriksa apakah form register telah disubmit
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST["username"];
    $password = $_POST["password"];

    // Memeriksa apakah username sudah ada dalam database
    $checkUsernameQuery = "SELECT * FROM user WHERE username = '$username'";
    $checkUsernameResult = $conn->query($checkUsernameQuery);

    if ($checkUsernameResult->num_rows > 0) {
        // Jika username sudah ada, tampilkan pesan error
        echo '<div class="alert alert-danger">Username sudah digunakan. Silakan pilih username lain.</div>';
    } else {
        // Jika username belum ada, tambahkan data ke tabel user
        $insertUserQuery = "INSERT INTO user (username, password) VALUES ('$username', '$password')";

        if ($conn->query($insertUserQuery) === TRUE) {
            // Registrasi berhasil, tampilkan pesan sukses
           // Registrasi berhasil, tampilkan pesan sukses
echo '<div class="alert alert-success">Registrasi berhasil. Silakan masuk dengan akun baru.</div>';

// Arahkan pengguna ke halaman login.php setelah registrasi berhasil
header("Location: login.php");
exit;

        } else {
            // Jika terjadi kesalahan saat menyimpan data, tampilkan pesan error
            echo '<div class="alert alert-danger">Terjadi kesalahan saat melakukan registrasi. Silakan coba lagi.</div>';
        }
    }
}

// Menutup koneksi ke database
$conn->close();
?>
```



![image.png](Domentasi/Create.png)

- **Read**

User dapat melihat akun yang telah terdaftar dari web di file user.php

```
<?php
include "koneksi.php";

// Query untuk mengambil data pengguna dari tabel "user"
$query = "SELECT * FROM user";
$result = mysqli_query($conn, $query);

?>

<!DOCTYPE html>
<html>
<head>
  <title>Daftar Pengguna</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <style>
    table {
      width: 100%;
      border-collapse: collapse;
    }

    th, td {
      padding: 8px;
      text-align: left;
      border-bottom: 1px solid #ddd;
    }

    th {
      background-color: #f2f2f2;
    }
  </style>
</head>
<body>
  <div class="container">
    <h1 class="mt-4">Daftar Pengguna</h1>

    <a href="index.php" class="btn btn-secondary mb-3"><i class="fas fa-arrow-left"></i> Kembali ke Beranda</a>

    <?php
    // Memeriksa apakah terdapat pengguna dalam tabel "user"
    if (mysqli_num_rows($result) > 0) {
      echo '<table class="table">';
      echo '<thead>';
      echo '<tr>';
      echo '<th>Password</th>';
      echo '<th>Username</th>';
      echo '<th>Aksi</th>';
      echo '</tr>';
      echo '</thead>';
      echo '<tbody>';

      // Menampilkan data pengguna
      while ($row = mysqli_fetch_assoc($result)) {
        echo '<tr>';
        echo '<td>' . $row['password'] . '</td>';
        echo '<td>' . $row['username'] . '</td>';
        echo '<td>';
        echo '<a href="editA.php?username=' . $row['username'] . '" class="btn btn-primary"><i class="fas fa-edit"></i> Edit</a> ';
        echo '<a href="hapusA.php?username=' . $row['username'] . '" class="btn btn-danger" onclick="return confirmDelete();"><i class="fas fa-trash"></i> Hapus</a>';
        echo '</td>';
        echo '</tr>';
      }

      echo '</tbody>';
      echo '</table>';
    } else {
      echo 'Tidak ada pengguna yang terdaftar.';
    }

    // Menutup koneksi ke database
    mysqli_close($conn);
    ?>
  </div>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.3/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <script>
    function confirmDelete() {
      return confirm("Apakah Anda yakin ingin menghapus akun ini?");
    }
  </script>
</body>
</html>
```



![image.png](Domentasi/read.png)

- **Update**

User dapat melihat akun yang telah terdaftar dari web di file user.php dan user dapat mengedit akun jika user merasa username atau password tidak sesuai yang akan di proses di file editA.php

```
**<?php
include "koneksi.php";

// Memeriksa apakah parameter "username" ada dalam URL
if (isset($_GET['username'])) {
  $id = $_GET['username'];

  // Query untuk mengambil data pengguna berdasarkan id
  $query = "SELECT * FROM user WHERE username = '$id'";
  $result = mysqli_query($conn, $query);

  // Memeriksa apakah data pengguna ditemukan
  if (mysqli_num_rows($result) > 0) {
    $user = mysqli_fetch_assoc($result);

    // Memeriksa apakah form edit telah disubmit
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $newUsername = $_POST["new_username"];
      $newPassword = $_POST["new_password"];

      // Query untuk memperbarui data pengguna
      $updateQuery = "UPDATE user SET username = '$newUsername', password = '$newPassword' WHERE username = '$id'";

      if (mysqli_query($conn, $updateQuery)) {
        // Pembaruan berhasil, redirect ke halaman daftar pengguna
        header("Location: user.php");
        exit();
      } else {
        echo '<div class="alert alert-danger">Terjadi kesalahan saat memperbarui data pengguna.</div>';
      }
    }
?>

<!DOCTYPE html>
<html>
<head>
  <title>Edit Pengguna</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>
  <div class="container">
    <h1 class="mt-4">Edit Pengguna</h1>

    <form method="post" action="">
      <div class="form-group">
        <label for="new_username">Username Baru:</label>
        <input type="text" class="form-control" name="new_username" value="<?php echo $user['username']; ?>" required>
      </div>
      <div class="form-group">
        <label for="new_password">Password Baru:</label>
        <input type="password" class="form-control" name="new_password" value="<?php echo $user['password']; ?>" required>
      </div>
      <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
  </div>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</body>
</html>

<?php
  } else {
    echo '<div class="alert alert-danger">Data pengguna tidak ditemukan.</div>';
  }
} else {
  echo '<div class="alert alert-danger">ID pengguna tidak diberikan.</div>';
}

// Menutup koneksi ke database
mysqli_close($conn);
?>**
```




![image.png](Domentasi/update.png)

- **Delete**

User dapat melakukan aksi delete akun jika merasa akun sudah tidak dibutuhkan lagi yang akan diproses di hapusA.php

```
<?php
include "koneksi.php";

// Memeriksa apakah parameter "id" ada dalam URL
if (isset($_GET['username'])) {
  $id = $_GET['username'];

  // Query untuk menghapus pengguna berdasarkan id
  $deleteQuery = "DELETE FROM user WHERE username = '$id'";

  if (mysqli_query($conn, $deleteQuery)) {
    // Penghapusan berhasil, redirect ke halaman daftar pengguna
    header("Location: user.php");
    exit();
  } else {
    echo '<div class="alert alert-danger">Terjadi kesalahan saat menghapus pengguna.</div>';
  }
} else {
  echo '<div class="alert alert-danger">ID pengguna tidak diberikan.</div>';
}

// Menutup koneksi ke database
mysqli_close($conn);
?>
```


![image.png](Domentasi/delete.png)




# 7. Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital

Pada Kode yang dipakai, terdapat beberapa komponen GUI yang umum ditemui:

- Menu Navigasi: Terdapat menu navigasi di bagian atas yang memungkinkan pengguna untuk beralih fitur yang berbeda.

- Konten Utama: Bagian ini merupakan area utama yang menampilkan informasi atau fungsi utama produk, seperti formulir, daftar, grafik, atau konten lainnya.

- Header: Header berisi judul atau logo produk yang mengidentifikasikan halaman atau fitur yang sedang digunakan.

- Tombol dan Tautan: GUI juga menyediakan tombol dan tautan interaktif yang memungkinkan pengguna untuk melakukan tindakan tertentu, seperti mengklik tombol "upload" pada menu uplaod file untuk upload file atau mengikuti tautan menuju halaman lain.

![image.png](Domentasi/websrvice.png)



# 8. Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital

1. URL Input:Memasukkan URL tujuan ke dalam bidang input ini. URL ini mewakili alamat tujuan untuk melakukan koneksi HTTP,"http://localhost/bays/login.php".

2. HTTP Methods: Terdapat pilihan metode HTTP yang tersedia, seperti GET, POST. 

3. Headers: Pengguna dapat menambahkan header tambahan yang akan dikirimkan dalam koneksi HTTP. Header ini berisi informasi tambahan yang dapat digunakan oleh server untuk memproses permintaan dengan benar.


# 9. Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube
https://youtu.be/dY7HN5AUofk

