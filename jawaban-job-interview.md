# 1. Mendemonstrasikan penggunaan Sistem Operasi untuk
- Monitoring resouce 

**RAM**
memori jangka pendek komputer atau laptop
untuk memonitor RAM dapat menggunakan command 

>`free`

![image.png](Domentasi/123.png)
dan untuk lebih rinci kita dapat menggunakan command --mega (megabyte) atau --giga (gigabyte)
untuk tampilan command `free --mega` dan `free --giga` sebagai berikut:
![image.png](Domentasi/free.png)

**CPU**
 yaitu prosesor yang dapat bertanggung jawab untuk menjalankan instruksi pada perangkat komputasi
untuk memonitor CPU dapat menggunakan command `top` atau `ps -aux`
untuk tampilan `ps -aux `sebagai berikut:
![image.png](Domentasi/ps_-_aux.png)
dan tampilan untuk penggunaan command `top` sebagai berikut:
![image.png](Domentasi/top.png)
**Hardisk**
 yaitu sebuah perangkat keras yang mempunyai fungsi untuk menyimpan sebuah data.
Untuk memonitor Hardisk di MobaXtrem kita dapat menggunakan command `df` atau `lsblk `
Untuk tampilan `df` sebagai berikut:
![image.png](Domentasi/df.png)

dan untuk tampilan command `lsblk` sebgai berikut:
![image.png](Domentasi/lsblk.png)

# 2. Manajemen Program
- Memonitor Program yang sedang berjalan

untuk memonitor program yang sedang berjalan dapat menggunakan command `top` atau `ps -aux` seperti diatas.


Untuk tampilan command top sebagai berikut:
![image.png](Domentasi/top.png)

- Menghentikan program yang sedang berjalan

Untuk mengentikan program yang sedang berlajan yaitu dengan cara masukan command 

>`kill (NO PID)`

![image.png](Domentasi/kill_ouput.png)
Selanjutnya masukan keyword `K` untuk perintah kill Lalu masukan no sinyalnya
- 1 (-HUP)   : untuk restart suatu proses.
- 9 (-KILL)  : untuk mematikan suatu proses.
- 15 (-TERM) : untuk menghentikan proses dengan cara berbeda.
![image.png](Domentasi/kill_ouput.png)

- Otomatisasi Perintah Dengan ShellScript

Untuk mengotomatisasi perintah dengan ShellScript pertama kita perrlu membuat file ShellScript dengan command `touch nama file.sh` lalu masukan perintah `vim` untuk masuk ke dalam file tersebut dan inputkan kode

>`#! /bin/bash/`

![image.png](Domentasi/vim.png)
Untuk mengeksekusi program tersebut bisa menggunakan    

>`./ namafile.sh `

Jika keluar ouput permisson denied,maka kita perlu mengubah hak akses file tersebut dengan menggunakan chmod yaitu dengan command

`ls -l` (berfungsi sebgai melihat hak akses dari file dan folder ) 

>`chmod (option) `


chmod diikuti dengan 3 angka (masing masing angka antara 0 sampai 7). 3 angka tersebut secara berurutan mempresentasikan permission atau izin dari owner, group, dan others

- r -> read -> 4
- w -> write -> 2
- x -> execute -> 1
![image.png](Domentasi/otomatisasishell.png)

- Penjadwalan Eksekusi Menggunakan Cron

Cron yaitu fitur di sistem operasi berbasis UNIX (Linux, Ubuntu, dan lain-lain) yang berfungsi untuk menjalankan tugas atau script secara otomatis.

Untuk membuat program cron yaitu dengan cara menggunakan command

>`crontab -e`

![image.png](Domentasi/crontab.png)
Untuk menambahkan program kita perlu masuk ke moder insert yaitu dengan cara `esc + i` lalu masukan command
 

 m h dom mon dow command 

>`* * *   *   *   /home/user/script.sh`

m yaitu minute,h yaitu hour, dom yaitu day of month,dow yaitu day of week.

Jika sudah maka selanjutnya masuk ke mode visual dengan cara `esc + v` dan untuk menyimpan program cron tersebut yaitu dengan mengetikan `:w` (di mode visual) dan untuk keluar program yaitu mengetikan `:q`

dan untuk melihat program crontab telah dibuat yaitu dengan menginputkan command `crontab -l`

![image.png](Domentasi/crontab2.png)


# 3. Manajemen Network
- Mengakses Sistem Operasi Menggunakan SSH

SSH client adalah aplikasi yang dipakai untuk menghubungkan sistem operasi dengan SSH server.
Untuk akses menggunakan SSH yaitu dengan perintah berikut:

>`ssh username@alamat_server`

![image.png](Domentasi/ssh.png)

- Memonitor Program Menggunakan Network

Untuk dapat memonitor Network kita perlu memasukan perintah `netstat`(Network Statistics) 

>`netstat`

![image.png](Domentasi/netsat.png)


- Mengoperasikan HTTP client
adalah perangkat lunak yang terutama digunakan untuk memungkinkan kita mengunduh file dari Internet. Alasan utama menggunakan klien HTTP umumnya untuk mengunduh file, tetapi ini juga dapat digunakan jika kita ingin melakukan debug atau berinteraksi dengan server web atau beberapa server web.Untuk mengoperasikan HTTP client yaitu dengan menggunakan perintah `curl` atau `wget`.

>`wget [option] [URL]`

![image.png](Domentasi/wget.png)

wget adalah mengunduh file tunggal dan menyimpannya di direktori working Anda saat ini.


> `curl [Options] [URL]`

Curl command adalah command yang tersedia di sebagian besar sistem berbasis Unix. Curl merupakan singkatan dari “Client URL”.

![image.png](Domentasi/curl.png)

# 4. Manajemen File Dan folder

- **Navigasi**

Untuk navigasi bisa menggunakan command berikut:

>`cd [Nama Folder]`

>`ls`

>`pwd`

cd, untuk mengganti direktori

ls, menampilkan semua list file dan folder yang ada di direktori yang sedang ditempati

pwd, menampilkan direktori yang sedang ditempati


![image.png](Domentasi/navigasi.png)

- **Crud File Dan Folder**

Untuk CRUD bisa menggunakan command berikut:

>`mkdir [Nama Direktori]`

>`touch [Nama File]` 

>`rm  [Nama File]` 

>`rmdir [Nama Folder]`

>`mv  [Nama File/Direktori] [Tujuan Direktori]`

>`cp  [Nama File/Direktori] [Tujuan Direktori]`

mkdir, untuk membuat direktori

touch, untuk membuat file

rm, menghapus file 

rmdir, menghapus direktori

mv, cut/move file atau direktori

cp, copy file atau direktori

![image.png](Domentasi/crud.png)

- Manajemen ownership
Ada dua command dasar dalam manajemen ownership yaitu chmod dan chown. 

CHMOD adalah kependekan dari CHange MODe, adalah merubah mode akses dari suatu file atau direktori 
adalah contoh penggunaannya:

>`chmod [option] <file>`

![image.png](Domentasi/chmod.png)


chmod, digunakan untuk memodifikasi permission suatu file.

`[option] `berisi perintah untuk mengubah permission

`<file>` berisi nama file yang akan diubah permissionnya.

`ls -l` (berfungsi sebgai melihat hak akses dari file dan folder )


Chown digunakan untuk mengubah pemilik dan grup pada sebuah file dan folder

>`chown [user:group] <file>`

![image.png](Domentasi/chown.png)

chown, digunakan untuk memodifikasi owner dari suatu file.

`[user:group]`, berisi nama user/group mana yang akan menjadi ownernya.

`file`,berisi nama file yang akan diubah ownernya

- **Pencarian File Dan Folder**

Untuk mencari file dan folder bisa menggunakan find atau grep. Contoh penggunaan find:

>`find <path> <conditions> <actions>`

![image.png](Domentasi/find.png)

`<directory>`, diisi dengan memasukkan direktori yang akan dicari file dan foldernya.

`<conditions>`, diisi dengan kondisi yang akan dipilih.

`<actions>`, diisi dengan actions yang akan dilakukan.


- Kompresi Data

Kompresi data atau pemampatan data adalah sebuah cara dalam ilmu komputer untuk memadatkan data sehingga hanya memerlukan ruangan penyimpanan lebih kecil.
Untuk melakukan kompresi data, bisa menggunakan command tar dan gzip, berikut contoh 
penggunaanya:

Tar -cf (untuk membuat file.tar)

Tar -xf (untuk membuka file.tar)
Dengan contoh untuk membuat file tar

>`tar [-cf] [fileku.tar] [file1 file2]`

![image.png](Domentasi/tarxf.jpg)

untuk membuka file tar tersebut gunakan command


>`Tar -xf [fileku.tar] [file1 file2]`

![image.png](Domentasi/tarxf1.jpg)

Zip merupakan format file arsip yang digunakan secara luas untuk mengompresi atau memampatkan satu atau beberapa file bersama-sama menjadi ke dalam satu lokasi
Dengan contoh yaitu dengan menggunakan file abc.JPG dengan total ukuran 40k 

![image.png](Domentasi/duhc.jpg)

lalu diubah menjadi zip

>`gzip [options] [filenames]`

![image.png](Domentasi/gzip.jpg)
maka zip tersebut berhasil dibuat

![image.png](Domentasi/gzipO.jpg)

Dan kita bisa melihat ukuran file setelah di zip dengan command `du`,`du h`,`du hc`.

![image.png](Domentasi/gzipOU.jpg)

Dan file telah berhasil di zip dengan format nama “abc.JPG.gz” dan ukurun nyapun telah berkurang menjadi 32k dari 40k. 

Untuk Mengekstrak file zip tersebut dapat menggunakan command

>`gunzip [options] [filenames]`

![image.png](Domentasi/gunzip.jpg)

![image.png](Domentasi/gunzipO.jpg)

Maka file abc.JPG.gz telah di ekstrak dan file asli abc.JPG telah Kembali ada 

# 5. Membuat Program Berbasis CLI Menggunakan ShellScript

Langkah pertama membuat `file.sh` terlebih dahulu dengan command 

>`touch [nama file]`

![image.png](Domentasi/1.jpg)

Setelah Itu kita lihat apakah file `tahubulat.sh` telah terbuat dengan menggunakan command 

>`ls`

![image.png](Domentasi/2.jpg)

Selanjutnya kita masuk ke mode `vim` dengan menggunakan command

>`vim [nama file]`

![image.png](Domentasi/3.jpg)



![image.png](Domentasi/4.jpg)

Jika telah masuk ke file tersebut kita perlu mengubah mode ke mode insert dengan menggunakan command

>`ESC + I`

lalu inputkan kode sesuai kebutuhan,setelah diinput yaitu menyimpan kodingan tersebut dengan cara masuk ke mode visul menggunakan command

>`ESC + V`

lalu inputkan `w` untuk menyimpan dan `q` untuk keluar vim

Selanjutkan untuk melihat apakah kodingan tersebut benar benar tersimpan atau tidak yaitu dengan menggunakan command

>`cat [nama file]`

![image.png](Domentasi/7.jpg)

Selanjutnya Kita eksekusi program tersebut dengan menggunakan command

>`./nama-file`

![image.png](Domentasi/5.jpg)
dan jika muncul notif "permission denied" maka kita harus mengubah hak akses file tersebut dengan melihat hak akses menggunakan command` ls -l` (melihat hak akses) 

![image.png](Domentasi/9.jpg)


dan command `chmod` untuk merubah hak akses file tersebut.

>`chmod [option] [nama file]`

![image.png](Domentasi/8.jpg)

Selanjutnya kita eksekusi kembali dan inputkan sesuai dengan programnya dengan command

>`./nama-file`

![image.png](Domentasi/6.jpg)


# 6. Demonstrasi program CLI dalam bentuk Youtube
[LINK](https://youtu.be/qlFnXV2AtRg)


# 7. Maze Challenge
![image.png](Domentasi/SATU.jpg)

![image.png](Domentasi/DUA.jpg)

![image.png](Domentasi/TIGA.jpg)

![image.png](Domentasi/EMPAT.jpg)

![image.png](Domentasi/LIMA.jpg)

![image.png](Domentasi/ENAM.jpg)

![image.png](Domentasi/tree.png)


